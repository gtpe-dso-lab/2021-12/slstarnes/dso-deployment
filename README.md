# DSO Course Deployment Configuration Example

This configuration is used by ArgoCD (https://argoproj.github.io/argo-cd/) to deploy our course applications.  We are also using Kustomize (https://github.com/kubernetes-sigs/kustomize) to override our container image tag.  

The pipeline template's deploy and release stages will automatically write the newest container tag, or release tag into the `kustomization.yaml` configuration for `stage` and `prod` overlays.

## Deployment Exercise

* Fork this repo, use the lab's namespace again - so we can assist if needed.
* Two options
    * use Gitpod to make changes, and commit back in with git - RECOMMENDED
    * clone locally to make changes, use git to push changes back up
* Copy the `flask-helloworld` folder in this repo and rename it to `flask-helloworld-<your student number>`
    * If you use Gitpod, use copy + paste keyboard shortcuts to create the duplicate `flask-helloworld` folder
* Within that folder, replace all instances of `flask-helloworld-NN` with `flask-helloworld-<your student number>` - Be sure to use the two-digit value - e.g, if you're student #1 use `01`
* Commit and push the changes to **your** fork
* Create a MR back to the source repository and add the course instructors as reviewers - the source is your repo, the destination is the one you forked from
* Once you've created the MR notify the course instructors, and they'll update the staging and production cluster configurations

